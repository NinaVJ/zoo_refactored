public class Small implements Size {

    private int value;

    public Small() {
        this.value = 1;
    }

    @Override
    public int getValue() {
        return value;
    }
}
