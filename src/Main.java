import java.util.*;

public class Main {

    public static void main(String[] args) {

        ArrayList<Animal> animalList = InputHandler.getInput();

        Train train = ZooKeeper.runAlgorithm(animalList);
    }

}
