public class Carnivore implements Animal {

    Size size;

    public Carnivore(Size size) {
        this.size = size;
    }

    @Override
    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public String toString() {
        switch (size.getValue()) {
            case 5:
                return "Large Carnivore";
            case 3:
                return "Medium Carnivore";
            case 1:
                return "Small Carnivore";
            default:
                return "This is wrong";
        }
    }
}
