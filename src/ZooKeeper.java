import java.util.ArrayList;
import java.util.List;

public class ZooKeeper {
    public static Train runAlgorithm(List<Animal> animals) {

        //create instance of train
        Train train = new Train();

        //counters:
        int largeCarnivores = 0;
        int mediumCarnivores = 0;
        int smallCarnivores = 0;
        int largeHerbivores = 0;
        int mediumHerbivores = 0;
        int smallHerbivores = 0;

        //get amount of animals per type and and weight
        for (Animal animal : animals) {
            int currentAnimalSize = animal.getSize().getValue();

       //Niet SOLID, weet niet hoe ik dit moet aanpassen
            switch (currentAnimalSize) {
                case 5:
                    if (animal instanceof Carnivore) largeCarnivores++;
                    else largeHerbivores++;
                    break;
                case 3:
                    if (animal instanceof Carnivore) mediumCarnivores++;
                    else mediumHerbivores++;
                    break;
                case 1:
                    if (animal instanceof Carnivore) smallCarnivores++;
                    else smallHerbivores++;
                    break;
                default:
                    System.out.println("Something went wrong");
            }
        }

        System.out.println("there are " + largeCarnivores + " Large Carnivores,  " + largeHerbivores + " Large Herbivores, "
                + mediumCarnivores + " Medium Carnivores, " + mediumHerbivores + " Medium Herbivores, " + smallCarnivores
                + " Small Carnivores and " + smallHerbivores + " Small Herbivores");

        //Step 1: put all Large Carnivores in separate wagons
        for (Animal possibleLargeCarnivore :
                new ArrayList<Animal>(animals)) {
            if (possibleLargeCarnivore instanceof Carnivore && possibleLargeCarnivore.getSize() instanceof Large) {
                //instantiate a Wagon & add Animal to animalList in wagon (also increases weight)
                Wagon wagon = new Wagon();
                train.addWagon(wagon);
                wagon.addAnimal(possibleLargeCarnivore);
                animals.remove(possibleLargeCarnivore);
                //keep count of animals left to distribute among wagons
                largeCarnivores--;
                System.out.println(largeCarnivores + " large carnivores left.");
            }
        }

        //Step 2: Put all Medium Carnivores in separate wagons and combine with Large Herbivores until on of them runs out
        for (Animal possibleMediumCarnivore :
                new ArrayList<Animal>(animals)) {
            if (possibleMediumCarnivore instanceof Carnivore && possibleMediumCarnivore.getSize() instanceof Medium) {
                //instantiate a Wagon & add Animal to animalList in wagon (also increases weight)
                Wagon wagon = new Wagon();
                train.addWagon(wagon);
                wagon.addAnimal(possibleMediumCarnivore);
                animals.remove(possibleMediumCarnivore);
                mediumCarnivores--;
                System.out.println(mediumCarnivores + " medium carnivores left.");
                //add Large Herbivores to the same wagon as the Medium Carnivores
                for (Animal possibleLargeHerbivore :
                        new ArrayList<Animal>(animals)) {
                    if (possibleLargeHerbivore instanceof Herbivore && possibleLargeHerbivore.getSize() instanceof Large && 10 - wagon.getCurrentWagonWeight() >= 5) {
                        wagon.addAnimal(possibleLargeHerbivore);
                        animals.remove(possibleLargeHerbivore);
                        largeHerbivores--;
                        System.out.println(largeHerbivores + " large herbivores left");
                    }
                }
            }
        }

        //Step 3: Put all Small Carnivores in separate wagons and combine with herbivores

        for (Animal possibleSmallCarnivores : new ArrayList<Animal>(animals)) {
            if (possibleSmallCarnivores instanceof Carnivore && possibleSmallCarnivores.getSize() instanceof Small) {
                Wagon wagon = new Wagon();
                train.addWagon(wagon);
                wagon.addAnimal(possibleSmallCarnivores);
                animals.remove(possibleSmallCarnivores);
                smallCarnivores--;

                if (mediumHerbivores >= 3) {
                    for (Animal possibleMediumHerbivores : new ArrayList<Animal>(animals)) {
                        if (possibleMediumHerbivores instanceof Herbivore && possibleMediumHerbivores.getSize() instanceof Medium && 10 - wagon.getCurrentWagonWeight() >= 3) {
                            wagon.addAnimal(possibleMediumHerbivores);
                            animals.remove(possibleMediumHerbivores);
                            mediumHerbivores--;
                        }
                    }
                } else {
                    for (Animal possibleMediumHerbivores : new ArrayList<Animal>(animals)) {
                        if (possibleMediumHerbivores instanceof Herbivore && possibleMediumHerbivores.getSize() instanceof Medium && wagon.getCurrentWagonWeight() == 1) {
                            wagon.addAnimal(possibleMediumHerbivores);
                            animals.remove(possibleMediumHerbivores);
                            mediumHerbivores--;
                        }
                        if (possibleMediumHerbivores instanceof Herbivore && possibleMediumHerbivores.getSize() instanceof Large && wagon.getCurrentWagonWeight() <= 5) {
                            wagon.addAnimal(possibleMediumHerbivores);
                            animals.remove(possibleMediumHerbivores);
                            largeHerbivores--;
                        }
                    }
                }
            }
        }

        //Step 4: put the remaining herbivores in wagons (only herbivores left in the list from now on...)
        //put large herbivores in wagons in pairs, possibly one left if uneven number
        while (largeHerbivores > 1) {
            Wagon wagon = new Wagon();
            train.addWagon(wagon);
            for (Animal possibleLarge : new ArrayList<Animal>(animals)) {
                if (possibleLarge instanceof Large && wagon.getCurrentWagonWeight() <= 5) {
                    wagon.addAnimal(possibleLarge);
                    animals.remove(possibleLarge);
                    largeHerbivores--;
                }//possibly one Large herbivore left to put in wagon
            }
        }
        //put all medium herbivores in wagons three at a time if 3 or more medium carnivores are left
        while (mediumHerbivores >= 3) {
            Wagon wagon = new Wagon();
            train.addWagon(wagon);
            for (Animal possibleMedium : new ArrayList<Animal>(animals)) {
                if (possibleMedium.getSize() instanceof Medium && 10 - wagon.getCurrentWagonWeight() >= 3) {
                    wagon.addAnimal(possibleMedium);
                    animals.remove(possibleMedium);
                    mediumHerbivores--;
                }//possible 0 tot 2 medium herbivores left to put in wagon
            }

            //todo: add small herbivore
        }

        //if there is still 1 Large Herbivore put in separate wagon
        if (largeHerbivores == 1) {
            Wagon wagon = new Wagon();
            train.addWagon(wagon);
            for (Animal possibleLarge : new ArrayList<Animal>(animals)) {
                if (possibleLarge.getSize() instanceof Large) {
                    wagon.addAnimal(possibleLarge);
                    animals.remove(possibleLarge);
                    largeHerbivores--;
                }
            }
            //if medium herbivores left, put in the same wagon
            if (mediumHerbivores > 0) {
                for (Animal possibleMedium : new ArrayList<Animal>(animals)) {
                    if (possibleMedium instanceof Medium && 10 - wagon.getCurrentWagonWeight() >= 3) {
                        wagon.addAnimal(possibleMedium);
                        animals.remove(possibleMedium);
                        mediumHerbivores--;
                    }
                }
            }
            //if small herbivores left, put in the same wagon until wagon is full or no small herbivores left
            for (Animal possibleSmall : new ArrayList<Animal>(animals)) {
                if (possibleSmall instanceof Small && 10 - wagon.getCurrentWagonWeight() >= 1) {
                    wagon.addAnimal(possibleSmall);
                    animals.remove(possibleSmall);
                    smallHerbivores--;
                }
            }
        }//no large herbivores left

        //if any medium or small herbivores are left they will be distributed among wagons
        while (smallHerbivores + mediumHerbivores > 0) {
            Wagon wagon = new Wagon();
            train.addWagon(wagon);

            if (mediumHerbivores > 0) {
                for (Animal possibleMedium : new ArrayList<Animal>(animals)) {
                    if (possibleMedium instanceof Medium) {
                        wagon.addAnimal(possibleMedium);
                        animals.remove(possibleMedium);
                        mediumHerbivores--;
                    }
                }
            }
            //put all remaining animals (only small herbivores) in wagons
            while (wagon.getCurrentWagonWeight() < 10) {
                if (animals.size() > 0) {
                    wagon.addAnimal(animals.get(0));
                    animals.remove(0);
                    smallHerbivores--;
                } else break;
            }
        }


        System.out.println(" ");
        System.out.println("PRINTING TRAIN...");
        //Print entire train to console
        for (Wagon wagon : train.getWagons()) {
            System.out.println("Wagon: " + (train.getWagons().indexOf(wagon) + 1) + " - current weight: " + wagon.getCurrentWagonWeight());
            for (Animal animal : wagon.getAnimalList()) {
                System.out.println("Animal: " + animal.toString());
            }
            System.out.println(" ");
        }

        return train;
    }
}
