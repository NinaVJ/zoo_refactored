public class Herbivore implements Animal {

    Size size;

    public Herbivore(Size size) {
        this.size = size;
    }

    @Override
    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public String toString() {
        switch (size.getValue()) {
            case 5:
                return "Large Herbivore";
            case 3:
                return "Medium Herbivore";
            case 1:
                return "Small Herbivore";
            default:
                return "This is wrong";
        }
    }

}
