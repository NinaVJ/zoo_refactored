public class Medium implements Size {

    private int value;

    public Medium() {
        this.value = 3;
    }

    @Override
    public int getValue() {
        return value;
    }
}
