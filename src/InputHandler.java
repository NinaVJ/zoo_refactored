import enums.AnimalSize;
import enums.AnimalType;

import java.util.*;

public class InputHandler {

    public static ArrayList<Animal> getInput() {

        ArrayList<Animal> animalList = new ArrayList<Animal>();

        Scanner inputNumberOfAnimals = new Scanner(System.in);
        Scanner sizeScan = new Scanner(System.in);
        Scanner typeScan = new Scanner(System.in);


        System.out.println("Enter the number of animals to be shipped in wagons:");
        //numerical user input
        int numberOfAnimals = inputNumberOfAnimals.nextInt();

        //show user input
        System.out.println(numberOfAnimals + " animals need to be shipped in wagons without eating each other!");

        for (int i = 1; i <= numberOfAnimals; i++) {//start at 1 in sysout//todo fix this
            System.out.println("Is animal number " + i + " SMALL, MEDIUM or LARGE?:");
            String size = sizeScan.nextLine().toUpperCase();//convert to uppercase if lowercase

            System.out.println("Is animal number " + i + " a HERBIVORE or CARNIVORE:");
            String type = typeScan.nextLine().toUpperCase();//convert to uppercase if lowercase

            //add animal object
            if (type == "HERBIVORE") {
                switch (size) {
                    case "SMALL":
                        animalList.add(new Herbivore(new Small()));
                        break;
                    case "MEDIUM":
                        animalList.add(new Herbivore(new Medium()));
                        break;
                    case "LARGE":
                        animalList.add(new Herbivore(new Large()));
                        break;
                }
            } else if (type == "CARNIVORE") {
                switch (size) {
                    case "SMALL":
                        animalList.add(new Carnivore(new Small()));
                        break;
                    case "MEDIUM":
                        animalList.add(new Carnivore(new Medium()));
                        break;
                    case "LARGE":
                        animalList.add(new Carnivore(new Large()));
                        break;
                }
            }
        }

        return animalList;
    }
}