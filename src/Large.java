public class Large implements Size{

    private int value;

    public Large() {
        this.value = 5;
    }

    @Override
    public int getValue() {
        return value;
    }
}
